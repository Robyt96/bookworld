package rt.bookworld.exception;

public class InvalidObjectIdException extends Exception {
	private static final long serialVersionUID = 1L;

	public InvalidObjectIdException() {
		super();
	}

	public InvalidObjectIdException(String s) {
		super(s);
	}
}

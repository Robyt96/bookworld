package rt.bookworld.exception;

public class MissingAttributeException extends Exception {
	private static final long serialVersionUID = 1L;

	public MissingAttributeException() {
		super();
	}

	public MissingAttributeException(String s) {
		super(s);
	}
}

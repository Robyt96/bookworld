package rt.bookworld.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="Book") 
public class Book {
	@Id @Column(name="book_id") @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="title")
	private String title;
	
	@ManyToMany
	@JoinTable(
	    name="book_author",
	    joinColumns = { @JoinColumn(name="book_id") },
	    inverseJoinColumns = { @JoinColumn(name = "author_id") }
	)
	private List<Author> authors = new ArrayList<Author>();
	
	@Column(name="publicationYear")
	private int publicationYear;
	
	@Column(name="category")
	private String category;
	
	@Column(name="isbn")
	private String isbn;

	@Column(name="publisher")
	private String publisher;

	@ManyToMany
	@JoinTable(
	    name="book_library",
	    joinColumns = { @JoinColumn(name="book_id") },
	    inverseJoinColumns = { @JoinColumn(name = "library_id") }
	)
	private List<Library> libraries = new ArrayList<Library>();
	
	public Book() {
		
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}
	
	public void setAuthors(Author author) {
		List<Author> authors = new ArrayList<Author>();
		authors.add(author);
		
		this.authors = authors;
	}

	public int getPublicationYear() {
		return publicationYear;
	}

	public void setPublicationYear(int publicationYear) {
		this.publicationYear = publicationYear;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	
	public String getPublisher() {
		return publisher;
	}
	
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public List<Library> getLibraries() {
		return libraries;
	}

	public void setLibraries(List<Library> libraries) {
		this.libraries = libraries;
	}
	
	public void setLibraries(Library library) {
		List<Library> libraries = new ArrayList<Library>();
		libraries.add(library);
		
		this.libraries = libraries;
	}
}

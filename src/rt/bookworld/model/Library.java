package rt.bookworld.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="Library") 
public class Library {
	@Id @Column(name="library_id") @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="city")
	private String city;
	
	@Column(name="address")
	private String address;
	
	@Column(name="openingYear")
	private int openingYear;
	
	@ManyToMany(mappedBy="libraries")
	private List<Book> books = new ArrayList<Book>();
	
	@OneToMany
	@JoinColumn(name="library")
	private List<Employee> employees = new ArrayList<Employee>();

	public Library() {
		
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getOpeningYear() {
		return openingYear;
	}

	public void setOpeningYear(int openingYear) {
		this.openingYear = openingYear;
	}

	public List<Book> getBooks() {
		return books;
	}

	public List<Employee> getEmployees() {
		return employees;
	}
}

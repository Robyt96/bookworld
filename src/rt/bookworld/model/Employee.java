package rt.bookworld.model;

import javax.persistence.*;

@Entity
@Table(name="Employee") 
public class Employee {
	@Id @Column(name="employee_id") @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="firstname")
	private String firstname;
	
	@Column(name="lastname")
	private String lastname;
	
	@Column(name="birthYear")
	private int birthYear;
	
	@ManyToOne
	@JoinColumn(name="library")
	private Library library;
	
	@ManyToOne
	@JoinColumn(name="boss")
	private Employee boss;
	
	@Column(name="role")
	private String role;
	
	public Employee() {
		
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public int getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(int birthYear) {
		this.birthYear = birthYear;
	}

	public Library getLibrary() {
		return library;
	}

	public void setLibrary(Library library) {
		this.library = library;
	}

	public Employee getBoss() {
		return boss;
	}

	public void setBoss(Employee boss) {
		this.boss = boss;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}

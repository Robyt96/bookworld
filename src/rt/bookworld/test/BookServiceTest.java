package rt.bookworld.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import rt.bookworld.control.AuthorService;
import rt.bookworld.control.BookService;
import rt.bookworld.control.DatabaseUtils;
import rt.bookworld.control.LibraryService;
import rt.bookworld.exception.InvalidObjectIdException;
import rt.bookworld.exception.MissingAttributeException;
import rt.bookworld.model.Author;
import rt.bookworld.model.Book;
import rt.bookworld.model.Library;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BookServiceTest {
	BookService bookService = null;
	
	@BeforeClass
	public static void init() {
		DatabaseUtils.cleanTables();
	}
	
	@Before
	public void setup() {
		bookService = new BookService();
	}
	
	@After
	public void tearDown() {
		if (bookService != null) bookService.shutDown();
	}
	
	@Test
	public void test01_CreateBookWithMissingAttribute() {
		Book book = new Book();
		book.setTitle("Harry Potter");
		
		try {
			bookService.createBook(book);
			fail("Should throw an exception if one or more mandatory attributes are missing");
		} catch (MissingAttributeException e) {
			assertTrue(e.getMessage().equals("One or more mandatory attributes missing"));
		}
	}
	
	@Test
	public void test02_CreateBook() throws MissingAttributeException {
		AuthorService authorService = new AuthorService();
		LibraryService libraryService = new LibraryService();
		
		Author author1 = new Author();
		author1.setFirstname("Carlo");
		author1.setLastname("Fruttero");
		author1.setBirthYear(1926);
		
		Author author2 = new Author();
		author2.setFirstname("Franco");
		author2.setLastname("Lucentini");
		author2.setBirthYear(1920);
		
		List<Author> authors = new ArrayList<Author>();
		authors.add(author1);
		authors.add(author2);
		authorService.createAuthor(author1);
		authorService.createAuthor(author2);
		authorService.shutDown();
		
		Library library = new Library();
		library.setCity("Monza");
		library.setAddress("via Giuliani, 1");
		libraryService.createLibrary(library);
		libraryService.shutDown();
		
		Book book = new Book();
		book.setTitle("Il quarto libro della fantascienza");
		book.setCategory("racconti");
		book.setPublisher("Einaudi");
		book.setIsbn("8806131613");
		book.setPublicationYear(1991);
		book.setLibraries(library);
		book.setAuthors(authors);
		
		bookService.createBook(book);
		
		assertTrue(book.getId() > 0);
	}
	
	@Test
	public void test03_GetBookByIdWithInvalidId() {
		try {
			bookService.getBookById(0);
			fail("Should throw an exception if no book in DB was found");
		} catch (InvalidObjectIdException e) {
			assertTrue(e.getMessage().equals("Book not found"));
		}
	}
	
	@Test
	public void test04_GetBooks() {
		//get books by title
		List<Book> books = bookService.getBooksByTitle("Il quarto libro%");
		assertTrue(books.size() == 1);
		Book book1 = books.get(0);
		
		//get book by ISBN
		Book book2 = bookService.getBookByIsbn("8806131613");
		
		assertTrue(book1.getId() == book2.getId());
		assertTrue(book1.getAuthors().size() == 2);
	}
	
	@Test
	public void test05_UpdateBook() throws MissingAttributeException, InvalidObjectIdException {
		AuthorService authorService = new AuthorService();
		Author author = new Author();
		author.setLastname("Rowling");
		authorService.createAuthor(author);
		authorService.shutDown();
		
		LibraryService libraryService = new LibraryService();
		Library library = new Library();
		library.setCity("Monza");
		library.setAddress("via Giuliani, 1");
		libraryService.createLibrary(library);
		libraryService.shutDown();
		
		Book book = new Book();
		book.setTitle("Harry Potter");
		book.setIsbn("9788893814508");
		book.setPublisher("Salani");
		book.setCategory("horror");
		book.setPublicationYear(2018);
		book.setAuthors(author);
		book.setLibraries(library);
		
		bookService.createBook(book);
		long id = book.getId();
		
		Book book2 = bookService.getBookById(id);
		assertTrue(book2.getTitle().equals("Harry Potter"));
		
		book.setCategory("fantasy");
		book.setPublicationYear(2005);
		bookService.updateBook(book);
		
		assertTrue(book2.getTitle().equals("Harry Potter"));
		assertTrue(book2.getCategory().equals("fantasy"));
		assertTrue(book2.getPublicationYear() == 2005);
	}
	
	@Test
	public void test06_DeleteBook() throws InvalidObjectIdException {
		Book book = bookService.getBooksByTitle("Il quarto%").get(0);
		long id = book.getId();
		
		bookService.deleteBook(book);
		
		try {
			bookService.getBookById(id);
			fail("Should throw an exception if book was correctly deleted");
		} catch (InvalidObjectIdException e) {
			assertTrue(e.getMessage().equals("Book not found"));
		}
		
		AuthorService autorService = new AuthorService();
		List<Author> author = autorService.getAuthorsByName("Fruttero");
		autorService.shutDown();
		
		// check if deleted book's author still esists
		assertTrue(author.size() == 1);
	}
}

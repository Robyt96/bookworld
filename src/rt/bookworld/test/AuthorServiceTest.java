package rt.bookworld.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import rt.bookworld.control.AuthorService;
import rt.bookworld.control.BookService;
import rt.bookworld.control.DatabaseUtils;
import rt.bookworld.control.LibraryService;
import rt.bookworld.exception.InvalidObjectIdException;
import rt.bookworld.exception.MissingAttributeException;
import rt.bookworld.model.Author;
import rt.bookworld.model.Book;
import rt.bookworld.model.Library;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AuthorServiceTest {
	AuthorService authorService = null;
	
	@BeforeClass
	public static void init() {
		DatabaseUtils.cleanTables();
	}
	
	@Before
	public void setup() {
		authorService = new AuthorService();
	}
	
	@After
	public void tearDown() {
		if (authorService != null) authorService.shutDown();
	}
	
	@Test
	public void test01_CreateAuthorWithMissingAttribute() {
		Author author = new Author();
		
		try {
			authorService.createAuthor(author);
			fail("Should throw an exception if one or more mandatory attributes are missing");
		} catch (MissingAttributeException e) {
			assertTrue(e.getMessage().equals("Missing lastname"));
		}
	}
	
	@Test
	public void test02_CreateAuthor() throws MissingAttributeException, InvalidObjectIdException {
		Author author = new Author();
		author.setFirstname("Alessandro");
		author.setLastname("Manzoni");
		author.setNationality("italiana");
		author.setBirthYear(1785);
		
		authorService.createAuthor(author);

		long id = author.getId();
		assertTrue(id > 0);

		Author author2 = new Author();
		author2.setFirstname("Giuseppe");
		author2.setLastname("Manzoni");
		author2.setNationality("italiana");
		author2.setBirthYear(1973);
		
		authorService.createAuthor(author2);
		
		Author author3 = authorService.getAuthorById(id);
		assertTrue(author3.getFirstname().equals("Alessandro"));
	}
	
	@Test
	public void test03_GetAuthorByIdWithInvalidId() {
		try {
			authorService.getAuthorById(0);
			fail("Should throw an exception if no author in DB was found");
		} catch (InvalidObjectIdException e) {
			assertTrue(e.getMessage().equals("Author not found"));
		}
	}
	
	@Test
	public void test04_GetAuthorsByName() {
		List<Author> authors = authorService.getAuthorsByName("Manzoni");
		assertTrue(authors.size() == 2);
	}
	
	@Test
	public void test05_GetBooksByAuthorName() throws MissingAttributeException {
		BookService bookService = new BookService();
		LibraryService libraryService = new LibraryService();
		
		Library library = new Library();
		library.setCity("Monza");
		libraryService.createLibrary(library);
		libraryService.shutDown();
		
		Book book1 = new Book();
		book1.setTitle("I promessi sposi");
		book1.setIsbn("123456");
		book1.setLibraries(library);
		List<Author> author1 = authorService.getAuthorsByName("Manzoni", "Alessandro");
		book1.setAuthors(author1);
		
		Book book2 = new Book();
		book2.setTitle("Autobiografia");
		book2.setIsbn("654321");
		book2.setLibraries(library);
		List<Author> author2 = authorService.getAuthorsByName("Manzoni", "Giuseppe");
		book2.setAuthors(author2);
		
		bookService.createBook(book1);
		bookService.createBook(book2);
		bookService.shutDown();
		
		List<Book> books = authorService.getBooksByAuthorName("Manzoni");
		assertTrue(books.size() == 2);
	}
	
	@Test
	public void test06_UpdateAuthor() throws MissingAttributeException, InvalidObjectIdException {
		Author author = new Author();
		author.setFirstname("Roberto");
		author.setLastname("Alighieri");
		author.setNationality("italiana");
		author.setBirthYear(1265);
		
		authorService.createAuthor(author);
		assertTrue(author.getFirstname().equals("Roberto"));
		
		author.setFirstname("Dante");
		authorService.updateAuthor(author);
		long id = author.getId();
		
		Author author2 = authorService.getAuthorById(id);
		assertTrue(author2.getFirstname().equals("Dante"));
	}
	
	@Test
	public void test07_DeleteAuthor() {
		List<Author> authors = authorService.getAuthorsByName("Alighieri", "Dante");
		assertTrue(authors.size() == 1);
		
		authorService.deleteAuthor(authors.get(0));

		List<Author> authors2 = authorService.getAuthorsByName("Alighieri", "Dante");
		assertTrue(authors2.size() == 0);
	}
}

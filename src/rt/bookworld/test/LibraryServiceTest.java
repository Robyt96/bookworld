package rt.bookworld.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import rt.bookworld.control.AuthorService;
import rt.bookworld.control.BookService;
import rt.bookworld.control.DatabaseUtils;
import rt.bookworld.control.LibraryService;
import rt.bookworld.exception.InvalidObjectIdException;
import rt.bookworld.exception.MissingAttributeException;
import rt.bookworld.model.Author;
import rt.bookworld.model.Book;
import rt.bookworld.model.Library;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LibraryServiceTest {
	LibraryService libraryService = new LibraryService();
	
	@BeforeClass
	public static void init() {
		DatabaseUtils.cleanTables();
	}
	
	@Before
	public void setup() {
		libraryService = new LibraryService();
	}
	
	@After
	public void tearDown() {
		if (libraryService != null) libraryService.shutDown();
	}
	
	@Test
	public void test01_CreateLibraryWithMissingAttribute() {
		Library library = new Library();
		
		try {
			libraryService.createLibrary(library);
			fail("Should throw an exception if one or more mandatory attributes are missing");
		} catch (MissingAttributeException e) {
			assertTrue(e.getMessage().equals("City attribute missing"));
		}
	}
	
	@Test
	public void test02_CreateLibrary() throws MissingAttributeException {
		Library library1 = new Library();
		library1.setCity("Monza");
		library1.setAddress("via Giuliani, 1");
		library1.setOpeningYear(1965);
		
		Library library2 = new Library();
		library2.setCity("Lissone");
		library2.setAddress("piazza IV novembre, 2");
		library2.setOpeningYear(1973);
		
		libraryService.createLibrary(library1);
		libraryService.createLibrary(library2);
		
		assertTrue(library1.getId() > 0);
		assertTrue(library2.getId() > 0);
	}
	
	@Test
	public void test03_GetAllLibraries() {
		List<Library> libraries = libraryService.getAllLibraries();
		
		assertTrue(libraries.size() == 2);
		assertTrue(libraries.get(0).getCity().equals("Monza"));
	}
	
	@Test
	public void test03_UpdateLibrary() 
	throws InvalidObjectIdException, MissingAttributeException {
		List<Library> libraries = libraryService.getAllLibraries();
		Library library = libraries.get(0);
		library.setAddress("Via Giuliani, 1A");
		library.setOpeningYear(1962);
		long id = library.getId();
		
		libraryService.updateLibrary(library);
		
		Library lib2 = libraryService.getLibraryById(id);
		
		assertTrue(lib2.getCity().equals("Monza"));
		assertTrue(lib2.getAddress().equals("Via Giuliani, 1A"));
		assertTrue(lib2.getOpeningYear() == 1962);
	}
	
	@Test
	public void test04_DeleteLibrary() throws MissingAttributeException {
		List<Library> libraries = libraryService.getAllLibraries();
		
		AuthorService authorService = new AuthorService();
		Author author = new Author();
		author.setLastname("Alighieri");
		author.setFirstname("Dante");
		authorService.createAuthor(author);
		authorService.shutDown();
		
		BookService bookService = new BookService();
		Book book = new Book();
		book.setTitle("La divina commedia");
		book.setIsbn("333333");
		book.setLibraries(libraries);
		book.setAuthors(author);
		bookService.createBook(book);
		
		libraryService.deleteLibrary(libraries.get(0));
	    libraries = libraryService.getAllLibraries();	
	    assertTrue(libraries.size() == 1);
	    
	    List<Book> books = bookService.getBooksByTitle("La divina commedia");
	    assertTrue(books.size() == 1);
	    bookService.shutDown();
	}
}

package rt.bookworld.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import rt.bookworld.control.DatabaseUtils;
import rt.bookworld.control.EmployeeService;
import rt.bookworld.control.LibraryService;
import rt.bookworld.exception.InvalidObjectIdException;
import rt.bookworld.exception.MissingAttributeException;
import rt.bookworld.model.Employee;
import rt.bookworld.model.Library;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EmployeeServiceTest {
	EmployeeService employeeService = null;
	
	@BeforeClass
	public static void init() {
		DatabaseUtils.cleanTables();
	}
	
	@Before
	public void setup() {
		employeeService = new EmployeeService();
	}
	
	@After
	public void tearDown() {
		if (employeeService != null) employeeService.shutDown();
	}
	
	@Test
	public void test01_CreateEmployeeWithMissingAttribute() {
		Employee employee = new Employee();
		
		try {
			employeeService.createEmployee(employee);
			fail("Should throw an exception if one or more mandatory attributes are missing");
		} catch (MissingAttributeException e) {
			assertTrue(e.getMessage().equals("Employee lastname missing"));
		}
	}
	
	@Test
	public void test02_CreateEmployee() throws MissingAttributeException {
		LibraryService libraryService = new LibraryService();
		Library library = new Library();
		library.setCity("Desio");
		libraryService.createLibrary(library);
		libraryService.shutDown();
		
		Employee employee1 = new Employee();
		employee1.setFirstname("Mario");
		employee1.setLastname("Rossi");
		employee1.setBirthYear(1968);
		employee1.setRole("Capo biblioteca");
		employee1.setLibrary(library);
		
		employeeService.createEmployee(employee1);
		long id1 = employee1.getId();
		assertTrue(id1 > 0);
		
		Employee employee2 = new Employee();
		employee2.setFirstname("Gino");
		employee2.setLastname("Verdi");
		employee2.setBirthYear(1981);
		employee2.setRole("bibliotecario");
		employee2.setLibrary(library);
		employee2.setBoss(employee1);
		
		employeeService.createEmployee(employee2);
		long id2 = employee2.getId();
		assertTrue(id2 > 0);
	}
	
	@Test
	public void test03_GetEmployeeByIdWithInvalidId() {
		try {
			employeeService.getEmployeeById(0);
			fail("Should throw an exception if no employee in DB was found");
		} catch (InvalidObjectIdException e) {
			assertTrue(e.getMessage().equals("Employee not found"));
		}
	}
	
	@Test
	public void test04_GetEmployees() {
		List<Employee> employees = employeeService.getEmployeesByName("Verdi", "Gino");
		assertTrue(employees.size() == 1);
		
		Employee employee = employees.get(0);
		Employee boss = employee.getBoss();
		
		assertTrue(boss.getLastname().equals("Rossi"));
	}
	
	@Test
	public void test05_UpdateEmployee() throws InvalidObjectIdException, MissingAttributeException {
		List<Employee> employees = employeeService.getEmployeesByName("Verdi", "Gino");
		Employee employee = employees.get(0);
		employee.setRole("Vicecapo biblioteca");
		long id = employee.getId();
		employeeService.updateEmployee(employee);
		
		Employee employee2 = employeeService.getEmployeeById(id);
		
		assertTrue(employee2.getLastname().equals("Verdi"));
		assertTrue(employee2.getRole().equals("Vicecapo biblioteca"));
	}
	
	@Test
	public void test06_DeleteEmployee() throws InvalidObjectIdException {
		List<Employee> employees = employeeService.getEmployeesByName("Rossi");
		Employee boss = employees.get(0);
		
		try {
			employeeService.deleteEmployee(boss);
			fail("The deletion of the boss should fail because of the foreign key constraint");
		}
		catch (Exception ignore) { }
		
		employees = employeeService.getEmployeesByName("Rossi");
		assertTrue(employees.size() == 1);
		
		employees = employeeService.getEmployeesByName("Verdi");
		Employee employee = employees.get(0);
		long id = employee.getId();
		employeeService.deleteEmployee(employee);
		
		try {
			employeeService.getEmployeeById(id);
			fail("Should throw an exception if employee was correctly deleted");
		} catch (InvalidObjectIdException e) {
			assertTrue(e.getMessage().equals("Employee not found"));
		}
	}
}

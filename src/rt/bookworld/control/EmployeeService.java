package rt.bookworld.control;

import java.util.List;

import rt.bookworld.exception.InvalidObjectIdException;
import rt.bookworld.exception.MissingAttributeException;
import rt.bookworld.model.Employee;

public class EmployeeService {
	private EmployeeDAO eDAO;
	
	public EmployeeService() {
		eDAO = new EmployeeDAO();
	}
	
	public boolean validEmployee(Employee employee) {
		return employee.getLastname() != null;
	}
	
	public void createEmployee(Employee employee) throws MissingAttributeException {
		if (!validEmployee(employee)) {
			throw new MissingAttributeException("Employee lastname missing");
		}
		eDAO.createEmployee(employee);
	}
	
	public Employee getEmployeeById(long id) throws InvalidObjectIdException {
		Employee employee =  eDAO.getEmployeeById(id);
		if (employee == null) throw new InvalidObjectIdException("Employee not found");
		return employee;
	}
	
	public List<Employee> getEmployeesByName(String lastname) {
		return getEmployeesByName(lastname, "%");
	}

	public List<Employee> getEmployeesByName(String lastname, String firstname) {
		if (lastname == null) throw new IllegalArgumentException("Missing lastname");
		if (firstname == null) throw new IllegalArgumentException("Missing firstname");
		
		List<Employee> employee = eDAO.getEmployeesByName(lastname, firstname);
		
		return employee;
	}
	
	public void updateEmployee(Employee employee) throws MissingAttributeException {
		if (!validEmployee(employee)) throw new MissingAttributeException();
		eDAO.updateEmployee(employee);
	}
	
	public void deleteEmployee(Employee employee) {
		eDAO.deleteEmployee(employee);
	}
	
	public void shutDown() {
		if (eDAO != null) {
			eDAO.close();
		}
	}
}

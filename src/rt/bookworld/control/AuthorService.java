package rt.bookworld.control;

import java.util.List;

import rt.bookworld.exception.InvalidObjectIdException;
import rt.bookworld.exception.MissingAttributeException;
import rt.bookworld.model.Author;
import rt.bookworld.model.Book;

public class AuthorService {
	private AuthorDAO aDAO;
	
	public AuthorService() {
		aDAO = new AuthorDAO();
	}
	
	public boolean validAuthor(Author author) {
		return author.getLastname() != null;
	}
	
	public void createAuthor(Author author) throws MissingAttributeException {
		if (!validAuthor(author)) throw new MissingAttributeException("Missing lastname");
		aDAO.createAuthor(author);
	}
	
	public Author getAuthorById(long id) throws InvalidObjectIdException {
		Author author = aDAO.getAuthorById(id);
		if (author == null) throw new InvalidObjectIdException("Author not found");
		return author;
	}
	
	public List<Author> getAuthorsByName(String lastname) {
		return getAuthorsByName(lastname, "%");
	}

	public List<Author> getAuthorsByName(String lastname, String firstname) {
		if (lastname == null) throw new IllegalArgumentException("Missing lastname");
		if (firstname == null) throw new IllegalArgumentException("Missing firstname");
		
		List<Author> authors = aDAO.getAuthorsByName(lastname, firstname);
		
		return authors;
	}
	
	public List<Book> getBooksByAuthorName(String lastname) {
		return getBooksByAuthorName(lastname, "%");
	}
	
	public List<Book> getBooksByAuthorName(String lastname, String firstname) {
		if (lastname == null) throw new IllegalArgumentException("Missing lastname");
		if (firstname == null) throw new IllegalArgumentException("Missing firstname");
		
		List<Book> books = aDAO.getBooksByAuthorName(lastname, firstname);

		return books;
	}
	
	public void updateAuthor(Author author) throws MissingAttributeException {
		if (!validAuthor(author)) throw new MissingAttributeException("Missing lastname");
		aDAO.updateAuthor(author);
	}
	
	public void deleteAuthor(Author author) {
		// deleting an author should be avoided if exists at least one book
		// associated only with this author
		aDAO.deleteAuthor(author);
	}
	
	public void shutDown() {
		if (aDAO != null) {
			aDAO.close();
		}
	}
}

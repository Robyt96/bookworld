package rt.bookworld.control;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

import rt.bookworld.model.Author;
import rt.bookworld.model.Book;
import rt.bookworld.model.Library;

public class BookDAO {
	private static final String persistenceUnitName="BookworldDAO";
	private EntityManager em;
	private EntityManagerFactory factory;
	
	public BookDAO() {
		factory = Persistence.createEntityManagerFactory(persistenceUnitName);
		em = factory.createEntityManager();
	}
	
	public void createBook(Book book) {
		em.getTransaction().begin();
		em.persist(book);
		em.getTransaction().commit();
	}
	
	public Book getBookById(long id) {
		return em.find(Book.class, id);
	}
	
	public List<Book> getBooksByTitle(String title) {
		String query = "SELECT B FROM Book B WHERE B.title LIKE :title";
		List<Book> books = (List<Book>) em.createQuery(query, Book.class)
			.setParameter("title", title)
			.getResultList();

		return books;
	}
	
	public Book getBookByIsbn(String isbn) {
		String query = "SELECT B FROM Book B WHERE B.isbn = :isbn ORDER BY B.publicationYear DESC";
		Book book;
		
		try {
			book = (Book) em.createQuery(query, Book.class)
				.setParameter("isbn", isbn)
				.setMaxResults(1)
				.getSingleResult();
		}
		catch (NoResultException ex) {
			return null;
		}
		
		return book;
	}
	
	public void updateBook(Book book) {
		em.getTransaction().begin();
		em.merge(book);
		em.getTransaction().commit();
	}
	
	public void deleteBook(Book book) {
		em.getTransaction().begin();
		em.remove(book);
		em.getTransaction().commit();
	}
	
	public void close() {
		em.close();
		factory.close();
	}
	
	public static void main(String[] args) {
		Book book = new Book();
		book.setTitle("Autobiografia");
		book.setPublicationYear(2017);
		book.setCategory("biografia");
		book.setIsbn("12345678");
		
		LibraryDAO libdao = new LibraryDAO();
		Library library = libdao.getLibraryById(1);
		book.setLibraries(library);
		
		AuthorDAO authordao = new AuthorDAO();
		Author author = authordao.getAuthorById(2);
		book.setAuthors(author);
		
		BookDAO dao = new BookDAO();
		dao.createBook(book);
		
		//Book book2 = dao.getBookByIsbn("12345678");
		//System.out.println(book2.getTitle());
		
		authordao.close();
		libdao.close();
		dao.close();
	}

}

package rt.bookworld.control;

import java.util.List;

import rt.bookworld.exception.InvalidObjectIdException;
import rt.bookworld.exception.MissingAttributeException;
import rt.bookworld.model.Library;

public class LibraryService {
	private LibraryDAO lDAO;
	
	public LibraryService() {
		lDAO = new LibraryDAO();
	}
	
	public boolean validLibrary(Library library) {
		return library.getCity() != null;
	}
	
	public void createLibrary(Library library) throws MissingAttributeException {
		if (!validLibrary(library)) {
			throw new MissingAttributeException("City attribute missing");
		}
		lDAO.createLibrary(library);
	}
	
	public List<Library> getAllLibraries() {
		return lDAO.getAllLibraries();
	}
	
	public Library getLibraryById(long id) throws InvalidObjectIdException {
		Library library = lDAO.getLibraryById(id);
		if (library == null) throw new InvalidObjectIdException();
		return library;
	}
	
	public void updateLibrary(Library library) throws MissingAttributeException {
		if (!validLibrary(library)) throw new MissingAttributeException();
		lDAO.updateLibrary(library);
	}

	public void deleteLibrary(Library library) {
		// deleting a library should be avoided if exists at least one book
		// associated only with this library
		lDAO.deleteLibrary(library);
	}
	
	public void shutDown() {
		if (lDAO != null) {
			lDAO.close();
		}
	}
}

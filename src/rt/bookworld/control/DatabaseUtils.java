package rt.bookworld.control;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DatabaseUtils {
	private static final String persistenceUnitName="BookworldDAO";

	public static void cleanTables() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory(persistenceUnitName);;
		EntityManager em = factory.createEntityManager();
		
		em.getTransaction().begin();
		em.createNativeQuery("DELETE FROM book_library").executeUpdate();
		em.createNativeQuery("DELETE FROM book_author").executeUpdate();
		em.createNativeQuery("DELETE FROM book").executeUpdate();
		em.createNativeQuery("DELETE FROM author").executeUpdate();
		em.createNativeQuery("TRUNCATE TABLE employee").executeUpdate();
		em.createNativeQuery("DELETE FROM library").executeUpdate();
		em.getTransaction().commit();

		em.close();
		factory.close();
	}
}

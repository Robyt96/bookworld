package rt.bookworld.control;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import rt.bookworld.model.Library;

public class LibraryDAO {
	private static final String persistenceUnitName="BookworldDAO";
	private EntityManager em;
	private EntityManagerFactory factory;

	public LibraryDAO() {
		factory = Persistence.createEntityManagerFactory(persistenceUnitName);
		em = factory.createEntityManager();
	}
	
	public void createLibrary(Library library) {
		em.getTransaction().begin();
		em.persist(library);
		em.getTransaction().commit();
	}
	
	public List<Library> getAllLibraries() {
		List<Library> list = (List<Library>) em.createQuery("SELECT L FROM Library L", Library.class).getResultList();

		return list;
	}
	
	public Library getLibraryById(long id) {
		return em.find(Library.class, id);
	}
	
	public void updateLibrary(Library library) {
		em.getTransaction().begin();
		em.merge(library);
		em.getTransaction().commit();
	}

	public void deleteLibrary(Library library) {
		em.getTransaction().begin();
		em.remove(library);
		em.getTransaction().commit();
	}
	
	public void close() {
		em.close();
		factory.close();
	}

	public static void main(String[] args) {
		//Library library = new Library();
		//library.setCity("Milano");
		//library.setAddress("Via Verdi, 15");
		//library.setOpeningYear(2012);
		
		LibraryDAO dao = new LibraryDAO();
		//dao.createLibrary(library);

		Library lib = dao.getLibraryById(1);
		System.out.println(lib.getBooks().size());
		
		/*List<Library> list = dao.getAllLibraries();
		for (Library lib : list) {
			System.out.println(lib);
		}*/
		
		//dao.deleteLibrary(library);
		dao.close();
	}

}

package rt.bookworld.control;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import rt.bookworld.model.Employee;

public class EmployeeDAO {
	private static final String persistenceUnitName="BookworldDAO";
	private EntityManager em;
	private EntityManagerFactory factory;
	
	public EmployeeDAO() {
		factory = Persistence.createEntityManagerFactory(persistenceUnitName);
		em = factory.createEntityManager();
	}
	
	public void createEmployee(Employee employee) {
		em.getTransaction().begin();
		em.persist(employee);
		em.getTransaction().commit();
	}
	
	public Employee getEmployeeById(long id) {
		return em.find(Employee.class, id);
	}
	
	public List<Employee> getEmployeesByName(String lastname) {
		return getEmployeesByName(lastname, "%");
	}

	public List<Employee> getEmployeesByName(String lastname, String firstname) {
		String query = "SELECT E FROM Employee E WHERE E.lastname LIKE :lastname AND E.firstname LIKE :firstname";
		List<Employee> employees = (List<Employee>) em.createQuery(query, Employee.class)
			.setParameter("lastname", lastname)
			.setParameter("firstname", firstname)
			.getResultList();
		
		return employees;
	}

	public void updateEmployee(Employee employee) {
		em.getTransaction().begin();
		em.merge(employee);
		em.getTransaction().commit();
	}
	
	public void deleteEmployee(Employee employee) {
		em.getTransaction().begin();
		em.remove(employee);
		em.getTransaction().commit();
	}
	
	public void close() {
		em.close();
		factory.close();
	}
	
	public static void main(String[] args) {
		//Employee employee1 = new Employee();
		//employee1.setFirstname("Mario");
		//employee1.setLastname("Rossi");
		//employee1.setBirthYear(1981);
		
		//Employee employee2 = new Employee();
		//employee2.setFirstname("Gino");
		//employee2.setLastname("Verdi");
		//employee2.setBirthYear(1969);
		
		
		EmployeeDAO dao = new EmployeeDAO();
		//dao.createEmployee(employee1);
		//Employee boss = dao.getEmployeeyById(1);
		//employee2.setBoss(boss);
		//dao.createEmployee(employee2);
		Employee employee = dao.getEmployeeById(1);
		System.out.println(employee.getLastname());
		employee.setLastname("Bianchi");
		dao.updateEmployee(employee);
		Employee employeeNew = dao.getEmployeeById(1);
		System.out.println(employeeNew.getLastname());
		
		dao.close();
	}

}

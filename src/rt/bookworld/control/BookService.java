package rt.bookworld.control;

import java.util.List;

import rt.bookworld.exception.InvalidObjectIdException;
import rt.bookworld.exception.MissingAttributeException;
import rt.bookworld.model.Book;

public class BookService {
	private BookDAO bDAO;
	
	public BookService() {
		bDAO = new BookDAO();
	}
	
	public boolean validBook(Book book) {
		boolean validTitle = book.getTitle() != null;
		boolean validIsbn = book.getIsbn() != null;
		boolean validLibraries = book.getLibraries() != null && book.getLibraries().size() > 0;
		boolean validAuthors = book.getAuthors() != null && book.getAuthors().size() > 0;
		
		return validTitle && validLibraries && validAuthors && validIsbn;
	}
	
	public void createBook(Book book) throws MissingAttributeException {
		if (!validBook(book)) {
			throw new MissingAttributeException("One or more mandatory attributes missing");
		}
		bDAO.createBook(book);
	}
	
	public Book getBookById(long id) throws InvalidObjectIdException {
		Book book = bDAO.getBookById(id);
		if (book == null) throw new InvalidObjectIdException("Book not found");
		return book;
	}
	
	public List<Book> getBooksByTitle(String title) {
		if (title == null) throw new IllegalArgumentException("Missing title");
		return bDAO.getBooksByTitle(title);
	}
	
	public Book getBookByIsbn(String isbn) {
		if (isbn == null) throw new IllegalArgumentException("Missing ISBN");
		return bDAO.getBookByIsbn(isbn);
	}
	
	public void updateBook(Book book) throws MissingAttributeException {
		if (!validBook(book)) {
			throw new MissingAttributeException("One or more mandatory attributes missing");
		}
		bDAO.updateBook(book);
	}
	
	public void deleteBook(Book book) {
		bDAO.deleteBook(book);
	}
	
	public void shutDown() {
		if (bDAO != null) {
			bDAO.close();
		}
	}
}

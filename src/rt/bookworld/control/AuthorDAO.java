package rt.bookworld.control;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import rt.bookworld.model.Author;
import rt.bookworld.model.Book;

public class AuthorDAO {
	private static final String persistenceUnitName="BookworldDAO";
	private EntityManager em;
	private EntityManagerFactory factory;
	
	public AuthorDAO() {
		factory = Persistence.createEntityManagerFactory(persistenceUnitName);
		em = factory.createEntityManager();
	}
	
	public void createAuthor(Author author) {
		em.getTransaction().begin();
		em.persist(author);
		em.getTransaction().commit();
	}
	
	public Author getAuthorById(long id) {
		return em.find(Author.class, id);
	}

	public List<Author> getAuthorsByName(String lastname) {
		return getAuthorsByName(lastname, "%");
	}

	public List<Author> getAuthorsByName(String lastname, String firstname) {
		String query = "SELECT A FROM Author A WHERE A.lastname LIKE :lastname AND A.firstname LIKE :firstname";
		List<Author> authors = (List<Author>) em.createQuery(query, Author.class)
			.setParameter("lastname", lastname)
			.setParameter("firstname", firstname)
			.getResultList();
		
		return authors;
	}
	
	public List<Book> getBooksByAuthorName(String lastname) {
		return getBooksByAuthorName(lastname, "%");
	}
	
	public List<Book> getBooksByAuthorName(String lastname, String firstname) {
		List<Author> authors = getAuthorsByName(lastname, firstname);
		
		List<Book> books = new ArrayList<Book>();
		for (Author author : authors) {
			books.addAll(author.getBooks());
		}

		return books;
	}
	
	public void updateAuthor(Author author) {
		em.getTransaction().begin();
		em.merge(author);
		em.getTransaction().commit();
	}
	
	public void deleteAuthor(Author author) {
		em.getTransaction().begin();
		em.remove(author);
		em.getTransaction().commit();
	}
	
	public void close() {
		em.close();
		factory.close();
	}
	
	public static void main(String[] args) {
		Author author = new Author();
		author.setFirstname("Giuseppe");
		author.setLastname("Manzoni");
		author.setNationality("Italia");
		author.setBirthYear(1940);
		
		AuthorDAO dao = new AuthorDAO();
		
		//dao.createAuthor(author);
		//List<Book> books = dao.getBooksByAuthorName("manzoni");
		//for (Book book : books) {
		//	System.out.println(book.getTitle());
		//}
		dao.deleteAuthor(author);
		dao.close();
		
		//DatabaseUtils.cleanTables();
	}

}
